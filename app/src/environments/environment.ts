export const environment = {
    "envId": "0395a5a5-9278-2663-9868-c40ea8a27b3c",
    "name": "dev",
    "properties": {
        "production": false,
        "baseUrl": "/neutrinos",
        "tenantName": "neutrinos",
        "appName": "testingDragNdDrop",
        "namespace": "com.neutrinos.neutrinos.testingDragNdDrop",
        "isNotificationEnabled": false,
        "googleMapKey": "AIzaSyCSTnVwijjv0CFRA4MEeS-H6PAQc87LEoU",
        "firebaseSenderId": "FIREBASE_SENDER_ID",
        "firebaseAuthKey": "FIREBASE_AUTH_KEY",
        "authDomain": "FIREBASE_AUTH_DOMAIN",
        "databaseURL": "FIREBASE_DATABASE_URL",
        "storageBucket": "FIREBASE_STORAGE_BUCKET",
        "messagingSenderId": "FIREBASE_SENDER_ID",
        "appDataSource": "APP_DATA_SOURCE",
        "appAuthenticationStrategy": "basicAuth",
        "basicAuthUser": "username",
        "basicAuthPassword": "password"
    }
}