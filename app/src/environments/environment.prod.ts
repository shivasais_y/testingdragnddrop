export const environment = {
    "envId": "6cb96a6d-1e2d-d852-3a3e-9700b3eec1b9",
    "name": "prod",
    "properties": {
        "production": true,
        "baseUrl": "/neutrinos",
        "tenantName": "neutrinos",
        "appName": "testingDragNdDrop",
        "namespace": "com.neutrinos.neutrinos.testingDragNdDrop",
        "isNotificationEnabled": false,
        "googleMapKey": "AIzaSyCSTnVwijjv0CFRA4MEeS-H6PAQc87LEoU",
        "firebaseSenderId": "FIREBASE_SENDER_ID",
        "firebaseAuthKey": "FIREBASE_AUTH_KEY",
        "authDomain": "FIREBASE_AUTH_DOMAIN",
        "databaseURL": "FIREBASE_DATABASE_URL",
        "storageBucket": "FIREBASE_STORAGE_BUCKET",
        "messagingSenderId": "FIREBASE_SENDER_ID",
        "appDataSource": "APP_DATA_SOURCE",
        "appAuthenticationStrategy": "basicAuth",
        "basicAuthUser": "username",
        "basicAuthPassword": "password"
    }
}