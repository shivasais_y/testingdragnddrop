/*DEFAULT GENERATED TEMPLATE. DO NOT CHANGE SELECTOR TEMPLATE_URL AND CLASS NAME*/
import { Component, OnInit } from '@angular/core'
import { ModelMethods } from '../lib/model.methods';
import { BDataModelService } from '../service/bDataModel.service';

import { MatSnackBar } from "@angular/material/snack-bar";
import { DndDropEvent, DropEffect } from "ngx-drag-drop";
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { deleteconfirmationComponent } from '../deleteconfirmationComponent/deleteconfirmation.component'

/**
* Model import Example :
* import { HERO } from '../models/hero.model';
*/

/**
 * Service import Example :
 * import { HeroService } from '../services/hero/hero.service';
 */

@Component({
    selector: 'bh-dashboard',
    templateUrl: './dashboard.template.html'
})

export class dashboardComponent implements OnInit {
    dm: ModelMethods;
    
  draggableListLeft = [
    {
      content: "Strategy",
      effectAllowed: "copy",
      disable: false,
      handle: false,
      priority: 1,
      canChild: false,
      children:[]
    },
    {
      content: "Prespective",
      effectAllowed: "copy",
      disable: false,
      handle: false,
      priority: 2,
      canChild: true,
      children:[]
      
    },
    {
      content: "Objective",
      effectAllowed: "copy",
      disable: false,
      handle: false,
      priority: 3,
      canChild: true,
      children:[]
    },
    {
      content: "Goals",
      effectAllowed: "copy",
      disable: false,
      handle: true,
      priority: 4,
      canChild: true,
      children:[]
    }
  ];

  draggableListRight = [];
  rightListContent = [];
  
  layout:any;
  horizontalLayoutActive:boolean = false;
  private currentDraggableEvent:DragEvent;
  private currentDragEffectMsg:string;


  constructor(private bdms: BDataModelService, private snackBarService:MatSnackBar, public dialog: MatDialog) {
    this.dm = new ModelMethods(bdms);
  }

  
  onDragStart( event:DragEvent ) {

    this.currentDragEffectMsg = "";
    this.currentDraggableEvent = event;

    this.snackBarService.dismiss();
  }

  onDragged( item:any, list:any[], effect:DropEffect ) {   
    //console.log(item, list, effect);
    effect = item.effectAllowed;
    
    if( effect === "move" ) {
      const index = list.indexOf( item );
      list.splice( index, 1 );
    }
  }

  onDragEnd( event:DragEvent) { 
    
    this.currentDraggableEvent = event;
  }

  onDrop( event:DndDropEvent, list?:any[], item?:any ) { 
    let checkDuplicate = list.indexOf(event.data);  
    
    if(checkDuplicate >= 0){
      this.snackBarService.open('Duplicate Item(s) not allowed', undefined, {duration: 3000});      
      return false;
    }
    
    
    if(item && event.data.priority < item.priority){
      this.snackBarService.dismiss();
      this.snackBarService.open('"'+event.data.content+'" cant be child of "'+item.content+'"', undefined, {duration: 3000})
      return false;
    }
    
    this.rightListContent.push(event.data.content); //If only one Constraint apply can use this..
                                                       
    if( list && (event.dropEffect === "copy" || event.dropEffect === "move") ) {
      let index = event.index;
      if( typeof index === "undefined" ) {
        index = list.length;
      }
      list.splice( index, 0, event.data );
    }
    
  }
  
  removeItem(event, list, item){
    let checkIndex = list.indexOf(item);
    let helptext = '';
    if(item.children.length > 0){
       helptext = 'Note: This item('+item.content+') having children';
    }
    
    
    let dialogRef = this.dialog.open(deleteconfirmationComponent, {
      width: '350px',
      data: { helptext: helptext }
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result){
     	list.splice(checkIndex, 1);
    	this.snackBarService.open('Item(s) Deleted successfully.. !', undefined, {duration: 3000});
      }
    });
    
  }
  
  listSorted(list: any){
    //this.firstSortedList = list;
  }

  
    ngOnInit() {
      
    }
  	
    get(dataModelName, filter ?, keys ?, sort ?, pagenumber ?, pagesize ?) {
        this.dm.get(dataModelName, this, filter, keys, sort, pagenumber, pagesize,
            result => {
                // On Success code here
            },
            error => {
                // Handle errors here
            });
    }

    getById(dataModelName, dataModelId) {
        this.dm.getById(dataModelName, dataModelId,
            result => {
                // On Success code here
            },
            error => {
                // Handle errors here
            })
    }

    put(dataModelName, dataModelObject) {
        this.dm.put(dataModelName, dataModelObject,
            result => {
                // On Success code here
            }, error => {
                // Handle errors here
            })
    }

    validatePut(formObj, dataModelName, dataModelObject) {
        this.dm.validatePut(formObj, dataModelName, dataModelObject,
            result => {
                // On Success code here
            }, error => {
                // Handle errors here
            })
    }

    update(dataModelName, update, filter, options) {
        const updateObject = {
            update: update,
            filter: filter,
            options: options
        };
        this.dm.update(dataModelName, updateObject,
            result => {
                //  On Success code here
            }, error => {
                // Handle errors here
            })
    }

    delete (dataModelName, filter) {
        this.dm.delete(dataModelName, filter,
            result => {
                // On Success code here
            }, error => {
                // Handle errors here
            })
    }

    deleteById(dataModelName, dataModelId) {
        this.dm.deleteById(dataModelName, dataModelId,
            result => {
                // On Success code here
            }, error => {
                // Handle errors here
            })
    }

    updateById(dataModelName, dataModelId, dataModelObj) {
        this.dm.updateById(dataModelName, dataModelId, dataModelObj,
            result => {
                // On Success code here
            }, error => {
                // Handle errors here
            })
    }


}
